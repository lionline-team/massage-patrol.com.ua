<?php while (have_posts()) : the_post(); ?>
  <div class="row column">
    <nav aria-label="You are here:" role="navigation">
      <ul class="breadcrumbs">
        <?php yoast_breadcrumb( '<li>','</li>' ); ?>
      </ul>
    </nav>
  </div>

  <div class="row">
    <div class="content-block content blog-content column large-9">
      <div class="blog-item large-11">
        <div class="blog-item__content">
          <div class="blog-item__title"><?php the_title();?></div>
          <?php the_content();?>
        </div>
      </div>
    </div>
    <div class="sidebar_right column large-3">

      <?php get_search_form();?>

      <div class="categories"><span><?php _e('Categories','lionline');?></span>
        <ul>
          <?php
          $args = array(
            'title_li'  => "",
          );
          wp_list_categories($args) ;?>
        </ul>
      </div>
    </div>
  </div>
<?php endwhile; ?>
