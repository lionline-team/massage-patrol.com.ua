

<?php while (have_posts()) : the_post(); ?>
  <div class="row column">
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
        <?php yoast_breadcrumb( '<li>','</li>' ); ?>
         <!--  <li><a href="#">Головна </a></li>
          <li><a href="#">Типи Масажу </a></li>
          <li><span class="show-for-sr">Current: </span>Класичний з маслом</li> -->
        </ul>
      </nav>
    </div>

<?php endwhile; ?>
<div class="main">
  <div class="row">

    <div class="sidebar-left column large-3">
      <?php $categories = get_terms( "servises_cat", array(
        "orderby"    => "count",
        "hide_empty" => 0
      ) );
      ?>
      <?php
      if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
        echo '<ul>';
        foreach ( $categories as $term ) {
          ?>
          <li class=""><a href="<?php echo get_term_link( $term );?>"><?php echo $term->name;?></a></li>
        <?php }
        echo '</ul>';
      }
      ?>
    </div>
    <?php
    ?>
    <div class="content column large-8">
      <div class="categoryEntry clearfix">
        <div class="categoryEntry__title"><span><?php the_title();?></span></div>
        <div class="categoryEntry__img"><?php the_post_thumbnail('medium'); ?></div>
        <div class="content-block">
          <?php the_content();?>
        </div>
        <div class="price-item__price large-4 medium-4">
          <?php $description_massage_type=get_field('massage_types', pll_current_language('slug'));?>
          <ul>
            <?php $description_massage_type=get_field('massage_types', pll_current_language('slug'));?>
            <?php if( have_rows('prices') ):?>
              <?php while ( have_rows('prices') ) : ?>
                <?php the_row(); ?>
                <?php
                $type=get_sub_field('type');
                $key = array_search($type['value'], array_column($description_massage_type, 'id'));
                ?>
                <li>
                  <div class="info-icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/info_icon.svg" alt=""></div>
                  <div class="price-item__info">
                    <div class="info__close"><img src="<?php echo get_template_directory_uri();?>/dist/images/close_info.svg" alt=""></div>
                    <div class="info__title"><span><?php echo $description_massage_type[$key]['name']; ?></span></div>
                    <div class="info__text">
                      <p>
                        <?php echo $description_massage_type[$key]['text']; ?>
                      </p>
                    </div>
                  </div>
                  <span class="left"><?php echo $description_massage_type[$key]['name']; ?></span>
                  <span class="right"><?php the_sub_field('price', $queried_object);?><?php _e('грн','lionline');?></span>
                </li>
              <?php  endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>
        <?php get_template_part('templates/block','PopupForm'); ?>
      </div>
    </div>
  </div>
</div>
