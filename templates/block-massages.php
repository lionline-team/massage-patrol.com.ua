
<section>
  <div class="massage">
    <div class="row">
      <div class="title">
        <span><?php _e('Types of Massage','lionline');?></span>
      </div>
      <?php $categories = get_terms( "servises_cat", array(
        "hide_empty" => 0

      ) );
      ?>
      <?php
      if ( ! empty( $categories ) && ! is_wp_error( $categories ) ) {
        foreach ( $categories as $term ) {
          ?>
          <?php if (get_field('custom_show',$term)) continue;?>
          <article>
            <div class="massage-item clearfix">
              <div class="massage-item__img column large-6"><a href="<?php echo get_term_link( $term );?>"><img src="<?php the_field('cat_image',$term);?>" alt=""></a></div>
              <div class="massage-item__content column large-6">
                <div class="massage_title"><a href='<?php echo get_term_link( $term );?>'><span><?php echo $term->name;?></span></a></div>
                <div class="massage_text">
                  <p><?php echo $term->description;?></p>
                </div>
                <div class="massage_type hide-for-small-only">

                  <?php $args = array(
                    'post_type'              => array( 'services' ),
                    'orderby'               => array('date'),
                    'tax_query'              => array(
                      'relation' => 'AND',
                      array(
                        'taxonomy'         => 'servises_cat',
                        'terms'            => $term->term_id,
                      ),
                    ),
                  );

                  $query = new WP_Query( $args );
                  if ( $query->have_posts() ) {
                    echo '<ul  class="clearfix">';
                    while ( $query->have_posts() ) {
                      $query->the_post();
                      echo '<li><a href="'. get_permalink().'">' . get_the_title() . '</a></li>';
                    }
                    echo '</ul>';
                    wp_reset_postdata();
                  }
                  ?>
                </div>
                <div class="massage_btn"><a href="<?php echo get_term_link( $term );?>"><?php _e('Детальніше','lionline');?><i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
              </div>
            </div>
          </article>
          <?php
        }
      }
      ?>
    </div>
  </div>
</section>

<section>

  <?php $categories = get_terms( "servises_cat", array(
    "hide_empty" => 0
  ) );
  ?>
  <?php
  if ( ! empty( $categories ) && ! is_wp_error( $categories ) ) {
    foreach ( $categories as $term ) {
      ?>
      <?php if (!get_field('custom_show',$term)) continue;?>
      <div class="catering">
        <div class="row">
          <div class="title"><a href='<?php echo get_term_link( $term );?>'><span><?php echo $term->name;?></span></a>
            <div class="sub-title"><span><?php the_field('subtitle',$term);?></span></div>
          </div>
          <div class="catering-items ">
            <?php $args = array(
              'post_type'              => array( 'services' ),
              'order_by'               => array('default_date'),
              'tax_query'              => array(
                'relation' => 'AND',
                array(
                  'taxonomy'         => 'servises_cat',
                  'terms'            => $term->term_id,
                ),
              ),
            );

            $query = new WP_Query( $args );
            if ( $query->have_posts() ) {

              while ( $query->have_posts() ) : ?>
                <?php $query->the_post(); ?>
                <article class="column block-catering large-4 medium-6">
                  <div class="catering-item">
                    <div class="catering-item__img"><a href="<?php the_permalink();?>"><?php the_post_thumbnail('medium'); ?></a></div>
                    <div class="catering-item__title"><span><?php the_title();?></span></div>
                    <div class="catering-item__text">
                      <p><?php echo get_the_excerpt();?></p>
                    </div>
                    <div class="catering-item__btn"><a class="btn btn_transp" href="<?php the_permalink();?>"><? _e('Buy','lionline');?> </a></div>
                  </div>
                </article>


              <?php endwhile;

              wp_reset_postdata();
            }
            ?>

          </div>
          <div class="catering__btn">
            <a href="<?php echo get_term_link( $term );?>"><i class="fa fa-chevron-right" aria-hidden="true"></i><?php _e('Інші види кейтерінгу','lionline');?></a>
          </div>
        </div>
      </div>

      <?php
    }
  }
  ?>
</section>
