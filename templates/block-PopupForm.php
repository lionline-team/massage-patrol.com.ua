<!-- <div class="reveal" id="exampleModal1" data-reveal=""> -->
  <!-- <div class="reveal" id="exampleModal3" data-reveal="">
    <h2>ВІДПРАЛЕНО!!!</h2>
    <button class="close-button" data-close="" aria-label="Close reveal" type="button"><span aria-hidden="true">×</span></button>
  </div>  -->

  <div class="popupForm clearfix">
    <div class="success" style="display: none; margin-bottom: 40px;">
      <div class="title">
        <span><?php _E('Відправлено','lionline');?></span></div>
      </div>
      <form class='singleForm' id="popup">
        <div class="form">
          <div class="title"><span><?php _e('Sign up for Massage','lionline');?></span></div>
          <p><?php _e('form name','lionline');?>*</p>
          <input type="text" name='name' class="contactFormName">
          <p><?php _e('form email or phone','lionline');?>*</p>
          <input type="text" name='phone' class="contactFormPhone" required>
          <div class="full_form ">
            <p><?php _e('Місце','lionline');?></p>
            <select id="formAdrress" name="address" class='contactFormAddress'>
              <option><?php _e('Виїзд масажиста: Адресу напишіть в коментарі замовлення','lionline');?></option>
              <?php if( have_rows('bases', pll_current_language('slug')) ):?>
                <?php while ( have_rows('bases', pll_current_language('slug')) ) : ?>
                  <?php the_row(); ?>

                  <option>(<?php the_sub_field('city');?>) <?php the_sub_field('address', pll_current_language('slug'));?></option>

                <?php  endwhile; ?>
              <?php endif; ?>


              <?php if( have_rows('partners_base', pll_current_language('slug')) ):?>
                <?php while ( have_rows('partners_base', pll_current_language('slug')) ) : ?>
                  <?php the_row(); ?>
                  <option>(<?php the_sub_field('partners_name');?>) <?php the_sub_field('address', pll_current_language('slug'));?></option>
                <?php  endwhile; ?>
              <?php endif; ?>
            </select>


            <div class="popupForm__left column largfe-6 mediumf-6">
              <div class="inp_item inp_item_left">
                <p>Дата </p>
                <input type="text" name="date" class="contactFormDate" data-date-format="mm/dd/yy" id="dp2"/>
              </div>
              <div class="inp_item inp_item_right">
                <p><?php _e('Час','lionline');?></p>
                  <select id="formTime" name="time" class="contactFormTime">
                    <option>10.00</option>
                    <option>11.00</option>
                    <option>12.00</option>
                    <option>13.00</option>
                    <option>14.00</option>
                    <option>15.00</option>
                    <option>16.00</option>
                    <option>17.00</option>
                    <option>18.00</option>
                    <option>19.00</option>
                  </select>
              </div>
                <div class="inp_item inp_item_left">
                  <p><?php _e('Тип масажу','lionline');?></p>
                  <select id="type_Massage" name="type_Massage" class="contactFormType_Massage">

                    <!-- <option value="0"><?php _e('Types of Massage','lionline');?></option> -->
                    <?php $categories = get_terms( "servises_cat", array(
                      "orderby"    => "count",
                      "hide_empty" => 0

                    ) );
                    ?>
                    <?php

                    if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
                      foreach ( $categories as $term ) {
                        ?>
                        <option id="<?php echo $term->term_id ; ?>" data-massage='<?php echo $term->term_id ; ?>'> <?php echo $term->name;?></option>

                      <?php }

                    }
                    ?>
                  </select>

                </div>
                <div class="inp_item inp_item_right">
                  <p><?php _e('Duration of the session','lionline');?></p>

                  <select id="massage" name="massage" class="contactFormMassage">
                    <?php $categories = get_terms( "servises_cat", array(
                      "orderby"    => "count",
                      "hide_empty" => 0
                    ) );
                    ?>
                    <?php
                    if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
                      foreach ( $categories as $term ) { ?>
                        <?php $args = array(
                          'post_type'              => array( 'services' ),
                          'tax_query'              => array(
                            'relation'               => 'AND',
                            array(
                              'taxonomy'         => 'servises_cat',
                              'terms'            => $term->term_id,
                            ),
                          ),
                        );
                        $query = new WP_Query( $args );
                        if ( $query->have_posts() ) {
                          while ( $query->have_posts() ) {
                            $query->the_post();
                            echo '<option data-massage='.$term->term_id.'>' . get_the_title() . '</option>';
                          }
                          wp_reset_postdata();
                        } else {
                        }
                      }
                    }
                    ?>
                  </select>
                </div>

              </div>

              <!-- <input type="date" value="<?php echo date('Y-m-d') ;?>" class="contactFormDate"> -->



            <div class="popupForm__radio column">
              <div class="popupForm__radio__title">
                <p><?php _e('Категорія масажу','lionline');?></p>
              </div>
              <div class="input-items">
               <?php
               $description_massage_type=get_field('massage_types', pll_current_language('slug'));
               foreach ($description_massage_type as $massage) : ?>

                <div class="input-item large-4" id="block-<?= $massage['id'] ?>">
                  <input type="radio" name="type" id="<?= $massage['id'] ?>"  value="<?= $massage['id'] ?>"/>
                  <label for="<?= $massage['id'] ?>">
                    <div class="input-info">
                      <div class="info-icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/info_icon.svg" alt=""/></div>
                      <div class="input-item__info">
                        <div class="info__title"><span><?= $massage['name'] ?></span></div>
                        <div class="info__text">
                          <p><?= $massage['text'] ?></p>
                        </div>
                      </div>
                    </div>
                    <div class="type"><span><?= $massage['name'] ?></span></div>
                    <div class="input-item__icon"><i class="<?= $massage['icon_classes'] ?>"></i></div>
                    <div class="input-item__price"><span></span></div>
                  </label>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>


      </div>
      <p><?php _e('form comment','lionline');?></p>
        <textarea class="contactFormText" name="text" placeholder="<?php _e("Напишіть коментарій, більше деталей про стан вашого здоров'я, зручний час та місце для масажу та будь яку інформацію, яку вважатимете за потрібне. Ми це читаємо і це допоможе краще сфокусуватись на масажі для вас",'lionline');?>"></textarea>
      <span><?php _e('We call you back to clarify the details of the booking!','lionline');?></span>
      <div class="popupForm__btn">
        <button class="btn btn_yellow button success" type="submit"><?php _e('Sign up','lionline');?></button>
        <button class="btn" id="btn_resset" type="button"><?php _e('Cancel ','lionline');?></button>
      </div>
      <div class="allForm"><span id="allForm"><?php _e('Expanded recording form','lionline');?></span></div>
    </form>
    <style>
    .option-hide{
      display: none;
    }
  </style>
  <script>
    jQuery(document).ready(function() {

      <?php
      $description_massage_type=get_field('massage_types', pll_current_language('slug'));
      ?>

      load_massages();
      load_massage_prices();

      jQuery("#type_Massage").on("change", function() {
        load_massages();
        load_massage_prices()
      });

      jQuery("#massage").on("change", function() {
        load_massage_prices()
      });

    });

    function load_massages () {
      var jQueryselected_typeMassage = jQuery("#type_Massage > option:selected").data("massage");
      switch (jQueryselected_typeMassage) {

        <?php
        $categories = get_terms( "servises_cat", array(
          "orderby"    => "count",
          "hide_empty" => 0
        ) );
        if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
          foreach ( $categories as $term ) { ?>
            <?php $args = array(
              'post_type'              => array( 'services' ),
              'tax_query'              => array(
                'relation'               => 'AND',
                array(
                  'taxonomy'         => 'servises_cat',
                  'terms'            => $term->term_id,
                ),
              ),
            );
            $query = new WP_Query( $args );
            $options='';

            if ( $query->have_posts() ) {
              while ( $query->have_posts() ) {
                $query->the_post();
                $prices=array();

                if( have_rows('prices') ):?>
                  <?php while ( have_rows('prices') ) : ?>
                    <?php the_row(); ?>
                    <?php
                    $type=get_sub_field('type');
                    $price =  get_sub_field('price');
                    $prices[$type['value']]=$price;
                    ?>
                  <?php  endwhile; ?>
                <?php endif; ?>


                <?php
                  // var_dump($prices);
                $options.='<option data-prices='.json_encode($prices).' data-massage='.$term->term_id.'>' . get_the_title() . '</option>';
              }
              wp_reset_postdata();
            }
              //generate case for one massage type
            $case='case '.$term->term_id.':
            jQuery("#massage option").detach();
            jQuery("#massage").append(\''.$options.'\');
            break;';

            echo $case;
          }
        }
        ?>
      }
    }

    function load_massage_prices() {
      jQuery('.input-item').hide();

      var massage_prices = jQuery("#massage > option:selected").data('prices');

      if (Object.keys(massage_prices).length>0) {
        jQuery('.popupForm__radio').show();
        jQuery.each( massage_prices, function( key, value ) {
          jQuery('.input-item#block-'+key).show();
          var price=value+' <?php _e('грн','lionline');?>';
          jQuery('#block-' + key).find('.input-item__price span').html(price);
        });
      }
      else {
        jQuery('.popupForm__radio').hide();
      }
    }

  </script>
</div>

<script>
  jQuery( document ).ready(function() {

    jQuery(document).on('submit','form.singleForm',function(e){
      var name = jQuery(this).find('input.contactFormName').val();
      var phone = jQuery(this).find('input.contactFormPhone').val();
      var address = jQuery(this).find('select.contactFormAddress').val();
      var date = jQuery(this).find('input.contactFormDate').val();
      var time = jQuery(this).find('select.contactFormTime').val();
      var typeMassage = jQuery(this).find('select.contactFormType_Massage').val();
      var Massage = jQuery(this).find('select.contactFormMassage').val();
      var text = jQuery(this).find('textarea.contactFormText').val();
      var to = 'admin';
      console.log(name,phone,address,text,date,time,typeMassage,Massage);
      // ЗАПУСКАЙ АНІМАЦІЮ ТУТ

      // animateButton(jQuery(this).find('button'));

      jQuery(this).find('button').addClass('animate');
      jQuery(".singleForm").trigger("reset");

      jQuery.ajax({
        url: ajaxurl,
        data: {
          'action':'contactMassage',
          'name':name,
          'phone':phone,
          'address':address,
          'date':date,
          'time':time,
          'typeMassage':typeMassage,
          'Massage':Massage,
          'text':text,

          'to':to,
          'subscribe':'',
        },

        success:function(data) {
          console.log('sended!');
          jQuery('.singleForm').hide();
          jQuery('.success').css('display','block');
          // console.log(data);
          setTimeout(function(){
            jQuery('button').removeClass('animate');
          },3000);
          // ЗУПИНЯЙ АНІМАЦІЮ ТУТ
        },
        error: function(errorThrown){
          console.log(errorThrown);
        }
      });
      e.preventDefault(e);
    });


  });

</script>
