<div class="row">
	<div class="contact-form ">
		<form class='contactForm'>
			<div class="title"><span><?php _e('Do you have a question?','lionline');?></p></span>
				<div class="sub-title"><span><?php _e('We will answer you as soon as possible','lionline');?></span></div>
			</div>
			<p><?php _e('form name','lionline');?>*</p>
			<input type="text" class='contactFormName' name="name" placeholder="<?php _e('form name','lionline');?>">
			<p><?php _e('form email or phone','lionline');?></p>
			<input type="text" class='contactFormEmail' name="email" placeholder="<?php _e('form email or phone','lionline');?>" required>
			<p><?php _e('form Message','lionline');?></p>
			<textarea class='contactFormText' placeholder="<?php _e('form Message','lionline');?>" ></textarea>
			<div class="contact-form__btn">
				<button class="btn btn_yellow button success" type="submit"><?php _e('form button','lionline');?></button>
			</div>
		</form>
	</div>
</div>

<script>
	jQuery( document ).ready(function() {

    if ( typeof contact_form_home_hook_defined === 'undefined') { // No dublicate hooks, when use two forms on one page



    	jQuery(document).on('submit','form.contactForm',function(e){
    		var name = jQuery(this).find('input.contactFormName').val();
    		var email = jQuery(this).find('input.contactFormEmail').val();
    		var text = jQuery(this).find('textarea.contactFormText').val();
    		var to = 'admin';

        // ЗАПУСКАЙ АНІМАЦІЮ ТУТ

        // animateButton(jQuery(this).find('button'));

        jQuery(this).find('button').addClass('animate');
        jQuery(".contactForm").trigger("reset");
        
        jQuery.ajax({
        	url: ajaxurl,
        	data: {
        		'action':'sendmsg',
        		'name':name,
        		'email':email,
        		'text':text,
        		'to':to,
        		'subscribe':'',
        	},
        	success:function(data) {
        		console.log('sended!');

        		console.log(data);
        		setTimeout(function(){
        			jQuery('button').removeClass('animate');
        		},3000);
            // ЗУПИНЯЙ АНІМАЦІЮ ТУТ
        },
        error: function(errorThrown){
        	console.log(errorThrown);
        }
    }); 
        e.preventDefault(e);
    });
    	contact_form_home_hook_defined=true;

    }
});

</script>