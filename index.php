<div class="row column">
  <nav aria-label="You are here:" role="navigation">
    <ul class="breadcrumbs">
      <?php yoast_breadcrumb( '<li>','</li>' ); ?>
    </ul>
  </nav>
</div>
<div class="row">
  <h1 class="blog__title"><?= \Roots\Sage\Titles\title(); ?></h1>

  <div class="blog-content content-block column large-9">
    <?php the_archive_description( '<div class="rubriks-info large-11">', '</div>' ); ?>
    <?php while (have_posts()) : the_post(); ?>
      <article>
        <div class="blog-item large-11">
          <div class="blog-item__date"><span><?php echo get_the_date('j') ; ?> </span><span><?php echo get_the_date('M') ; ?></span></div>
          <div class="blog-item__content">
            <div class="blog-item__foto"> <a href="<?php echo get_permalink( );?>"><?php the_post_thumbnail('medium'); ?></a></div>
            <div class="posted-by"><span><?php the_category(', ', 'multiple') ;?></span></div>
            <div class="blog-item__title"><a href="<?php echo get_permalink( );?>"><?php the_title();?></a></div>
            <div class="blog-item__text">
              <p><?php echo get_the_excerpt();?> <a href="<?php echo get_permalink( );?>"><?php _e('Read more','lionline');?></a></p>
            </div>
          </div>
        </div>
      </article>
    <?php endwhile; ?>

    <div class="nav-links">
      <?php wp_pagenavi(); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/sidebar','' ); ?>
</div>
