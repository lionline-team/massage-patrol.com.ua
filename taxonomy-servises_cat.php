


  <div class="row column">
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
        <?php yoast_breadcrumb( '<li>','</li>' ); ?>
         <!--  <li><a href="#">Головна </a></li>
          <li><a href="#">Типи Масажу </a></li>
          <li><span class="show-for-sr">Current: </span>Класичний з маслом</li> -->
        </ul>
      </nav>
    </div>



<div class="main">

  <div class="row">
    <div class="sidebar-left column large-3">
      <?php $categories = get_terms( "servises_cat", array(
        "hide_empty" => 0
      ) );
      ?>
      <?php
      if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){
        echo '<ul>';
        foreach ( $categories as $term ) {
          ?>
          <li class=""><a href="<?php echo get_term_link( $term );?>"><?php echo $term->name;?></a></li>
        <?php } ?>
        <?php echo '</ul>'; ?>
      <?php } ?>
    </div>
    <?php
    $queried_object = get_queried_object();
    ?>
    <div class="content column large-8">
      <div class="category active" data-collection="1" data-target="0">
        <div class="category__title"><span><?php echo single_term_title();?></span></div>
        <div class="category__img"><img src="<?php the_field('cat_image', $queried_object);?>" alt=""></div>
        <div class="category__text">
          <p><?php echo  $queried_object->description;?></p>
        </div>
        <div class="price">
          <?php while (have_posts()) : the_post(); ?>
            <article>
              <div class="price-item clearfix">
                <div class="price-item__title"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
                <div class="price-item__text column large-7 medium-7">
                  <p><?php the_excerpt();?></p>
                  <div class="massage_btn"><a href="<?php echo get_permalink();?>"><?php _e('Детальніше','lionline');?><i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
                </div>

                <div class="price-item__price column large-5 medium-5">
                  <ul>
                    <?php $description_massage_type=get_field('massage_types', pll_current_language('slug'));?>
                    <?php if( have_rows('prices') ):?>
                      <?php while ( have_rows('prices') ) : ?>
                        <?php the_row(); ?>
                        <?php
                        $type=get_sub_field('type');
                        $key = array_search($type['value'], array_column($description_massage_type, 'id'));
                        ?>
                        <li>
                          <div class="info-icon"><img src="<?php echo get_template_directory_uri();?>/dist/images/info_icon.svg" alt=""></div>
                          <div class="price-item__info">
                            <div class="info__close"><img src="<?php echo get_template_directory_uri();?>/dist/images/close_info.svg" alt=""></div>
                            <div class="info__title"><span><?php echo $description_massage_type[$key]['name']; ?></span></div>
                            <div class="info__text">
                              <p>
                                <?php echo $description_massage_type[$key]['text']; ?>
                              </p>
                            </div>
                          </div>
                          <span class="left"><?php echo $description_massage_type[$key]['name']; ?></span>
                          <span class="right"><?php the_sub_field('price', $queried_object);?><?php _e('грн','lionline');?></span>
                        </li>
                      <?php  endwhile; ?>
                    <?php endif; ?>
                  </ul>
                  <div class="price-item__btn"><a class="btn btn_transp" data-open="exampleModal1" ><?php _e('Записатись на масаж','lionline');?></a></div>
                </div>
              </div>
            </article>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</div>
