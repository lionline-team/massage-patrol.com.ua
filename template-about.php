<?php
/**
 * Template Name: about Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<div class="row column">
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
        <?php yoast_breadcrumb( '<li>','</li>' ); ?>
         <!--  <li><a href="#">Головна </a></li>
          <li><a href="#">Типи Масажу </a></li>
          <li><span class="show-for-sr">Current: </span>Класичний з маслом</li> -->
        </ul>
      </nav>
    </div>
    <div class="row">
      <div class="about_title"><span><?php the_title() ?></span></div>

        <?php if( have_rows('about_post') ):?>
          <?php while ( have_rows('about_post') ) : ?>
            <?php the_row(); ?>

                <div class="about-blok clearfix">
                  <div class="about-blok__text column large-5 medium-12 small-12">
                    <div class="about-blok-title"><span><?php the_sub_field('about_title');?></span></div>
                    <?php the_sub_field('about_text');?>
                  </div>
                  <div class="about-blok__foto column large-7 medium-12 small-12"><img src="<?php the_sub_field('about_image');?>" alt=""></div>
                </div>

          <?php  endwhile; ?>
        <?php endif; ?>
    </div>

    <?php get_template_part('templates/block','specialization'); ?>
  
    
    <?php get_template_part('templates/block','OurTeam'); ?>



    <?php get_template_part('templates/block','reviews'); ?>

    <?php get_template_part('templates/block','partners'); ?>
<?php endwhile; ?>
