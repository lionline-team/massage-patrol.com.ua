<div class="search"><span><?php _e('Search by blog','lionline');?></span>
	<div class="search__input">
		<form  role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<input type="text" value="" name="s" id="s" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>">
		</form>
	</div>
</div>

