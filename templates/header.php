<header>
  <div class="header">
    <div class="header-top">
      <div class="mob_contact">
        <div class="icon_contact">
          <div class="burger_contact"></div>
        </div>
      </div>
      <div class="row  ">
        <div class="header__contact column large-8 medium-10">
          <ul>
            <?php if (get_field('contact_phone',pll_current_language('slug'))) : ?>
              <li>
                <a target="_blank" href="tel:<?php the_field('contact_phone',pll_current_language('slug'));?>"> <i class="fa fa-phone" aria-hidden="true"></i><?php the_field('contact_phone',pll_current_language('slug'));?></a>
              </li>
            <?php endif; ?>

            <?php if (get_field('contact_email',pll_current_language('slug'))) : ?>
              <li>
                <a target="_blank" href="mailto:<?php the_field('contact_email',pll_current_language('slug'));?>"> <i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('contact_email',pll_current_language('slug'));?></a>
              </li>
            <?php endif; ?>

            <?php if (get_field('contacts_facebook_link',pll_current_language('slug'))) : ?>
              <li>
                <a target="_blank" href="<?php the_field('contacts_facebook_link',pll_current_language('slug'));?>"> <i class="fab fa-facebook" aria-hidden="true"></i><span class="hide-for-medium-only"><?php the_field('contacts_facebook',pll_current_language('slug'));?></span></a>
              </li>
            <?php endif; ?>

            <?php if (get_field('contacts_instagram_link',pll_current_language('slug'))) : ?>
              <li>
                <a target="_blank" href="<?php the_field('contacts_instagram_link',pll_current_language('slug'));?>"> <i class="fab fa-instagram" aria-hidden="true"></i><span class="hide-for-medium-only"><?php the_field('contacts_instagram',pll_current_language('slug'));?></span></a>
              </li>
            <?php endif; ?>

          </ul>
        </div>
        <div class="header__search column large-4 medium-2">
          <div class="search" data-opener-target="search">
            <form  role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
              <label class="show-for-large" for="h-search"><?php _e('Search','lionline');?>:</label>
              <input type="search" id="h-search" value="" name="s" id="s" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" ><img class="btn-search" src="<?php echo get_template_directory_uri();?>/dist/images/serch-icon.svg" alt="" data-opener="search">
            </form>
          </div>
          <div class="lang clearfix">
            <select id="langSelect" onchange="if (this.value) window.location.href=this.value">
              <option value=""><?php echo pll_current_language('name');?></option>
              <?php
              $translations = pll_the_languages(array('raw'=>1,'hide_current'=>1));
              foreach ($translations as $translation) {
                echo '<option value="'.$translation['url'].'">'.$translation['name'].'</option>';
              }
              ?>
            </select>
          </div>
        </div>
      </div>
    </div>
    <div class="row header-bottom">
      <div class="header__logo column large-3 medium-3 small-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png" alt=""></a>
      </div>
      <div class="header__menu column large-9 medium-9 small-12">
        <div class="mob_open">
          <div class="icon">
            <div class="burger"></div>
          </div>
        </div>
        <nav>
          <ul class="nav-menu">
            <?php if (has_nav_menu('primary_navigation')) :?>
              <?php wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
            <?php endif;?>

          </ul>
        </nav>
        <div class="tel show-for-small-only"><a href="tel:<?php the_field('contact_phone','option');?>"><?php the_field('contact_phone','option');?></a></div>
      </div>
    </div>
  </div>
</header>
