<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('lionline', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navi' => __('Footerr Navigation', 'sage')

  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


add_filter('acf/settings/google_api_key', function () {
  return 'AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo';
});



// Register Custom Post Type
function add_servises_post_type() {

  $labels = array(
    'name'                  => _x( 'Services', 'Post Type General Name', 'lionline' ),
    'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'lionline' ),
    'menu_name'             => __( 'Services', 'lionline' ),
    'name_admin_bar'        => __( 'Service', 'lionline' ),
    'archives'              => __( 'Item Archives', 'lionline' ),
    'attributes'            => __( 'Item Attributes', 'lionline' ),
    'parent_item_colon'     => __( 'Parent Item:', 'lionline' ),
    'all_items'             => __( 'All Items', 'lionline' ),
    'add_new_item'          => __( 'Add New Item', 'lionline' ),
    'add_new'               => __( 'Add New', 'lionline' ),
    'new_item'              => __( 'New Item', 'lionline' ),
    'edit_item'             => __( 'Edit Item', 'lionline' ),
    'update_item'           => __( 'Update Item', 'lionline' ),
    'view_item'             => __( 'View Item', 'lionline' ),
    'view_items'            => __( 'View Items', 'lionline' ),
    'search_items'          => __( 'Search Item', 'lionline' ),
    'not_found'             => __( 'Not found', 'lionline' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'lionline' ),
    'featured_image'        => __( 'Featured Image', 'lionline' ),
    'set_featured_image'    => __( 'Set featured image', 'lionline' ),
    'remove_featured_image' => __( 'Remove featured image', 'lionline' ),
    'use_featured_image'    => __( 'Use as featured image', 'lionline' ),
    'insert_into_item'      => __( 'Insert into item', 'lionline' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'lionline' ),
    'items_list'            => __( 'Items list', 'lionline' ),
    'items_list_navigation' => __( 'Items list navigation', 'lionline' ),
    'filter_items_list'     => __( 'Filter items list', 'lionline' ),
  );
  $args = array(
    'label'                 => __( 'Service', 'lionline' ),
    'description'           => __( 'Post Type Description', 'lionline' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor','thumbnail','excerpt'  ),
    'taxonomies'            => array( 'servises_cat' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-exerpt-view',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'services', $args );

}
add_action( 'init',__NAMESPACE__ .  '\\add_servises_post_type', 0 );

// Register Custom Taxonomy
function ad_services_cat_tax() {

  $labels = array(
    'name'                       => _x( 'Categories', 'Taxonomy General Name', 'lionline' ),
    'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'lionline' ),
    'menu_name'                  => __( 'categories', 'lionline' ),
    'all_items'                  => __( 'All Items', 'lionline' ),
    'parent_item'                => __( 'Parent Item', 'lionline' ),
    'parent_item_colon'          => __( 'Parent Item:', 'lionline' ),
    'new_item_name'              => __( 'New Item Name', 'lionline' ),
    'add_new_item'               => __( 'Add New Item', 'lionline' ),
    'edit_item'                  => __( 'Edit Item', 'lionline' ),
    'update_item'                => __( 'Update Item', 'lionline' ),
    'view_item'                  => __( 'View Item', 'lionline' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'lionline' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'lionline' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'lionline' ),
    'popular_items'              => __( 'Popular Items', 'lionline' ),
    'search_items'               => __( 'Search Items', 'lionline' ),
    'not_found'                  => __( 'Not Found', 'lionline' ),
    'no_terms'                   => __( 'No items', 'lionline' ),
    'items_list'                 => __( 'Items list', 'lionline' ),
    'items_list_navigation'      => __( 'Items list navigation', 'lionline' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'servises_cat', array( 'services' ), $args );

}
add_action( 'init', __NAMESPACE__ .  '\\ad_services_cat_tax', 0 );




add_action('wp_head',__NAMESPACE__ . '\\pluginname_ajaxurl');
function pluginname_ajaxurl() {
  echo '<script type="text/javascript">var ajaxurl =\''.admin_url('admin-ajax.php').'\'</script>';
}



function send_message() {

  ini_set('display_errors',1); //show all php errors
  error_reporting(E_ALL);


  if ( isset($_REQUEST) ) {
    $text =$_REQUEST['text'];
    $name =$_REQUEST['name'];

    $email=$_REQUEST['email'];
  }
  $mail=get_option('admin_email');

  $date=date('d-m-Y');
  $title="Нове повідомлення (".$date.')';
  $body="В вас нове повідомлення від:".$name  . "<br>";



  if (strlen($email)>3)
    $body.="Email: <a href=\"mailto:".$email."\">".$email."</a><br>
  Text: ".$text."<br>";
  $headers = array(
    'Content-Type: text/html; charset=UTF-8',
    "From: ".get_option('siteurl')." <".$mail.">" . "\r\n");
  $sent_message = wp_mail( $mail, $title, $body, $headers );
  if ( $sent_message ) {
    // the message was sent...
    echo 'Повідомлення адміністратору відправлено ('.$name.':'.$text.')';
  } else {
    // the message was not sent...
    var_dump($_REQUEST);
    echo 'Повідомлення адміністратору не відправлено!</br>';
  }
  die();
}

add_action( 'wp_ajax_nopriv_sendmsg', __NAMESPACE__ . '\\send_message' );
add_action( 'wp_ajax_sendmsg', __NAMESPACE__ . '\\send_message' );





function MassageSend_message() {

  ini_set('display_errors',1); //show all php errors
  error_reporting(E_ALL);


  if ( isset($_REQUEST) ) {
    $text =$_REQUEST['text'];
    $name =$_REQUEST['name'];
    $phone =$_REQUEST['phone'];
    $address =$_REQUEST['address'];
    $date =$_REQUEST['date'];
    $time =$_REQUEST['time'];
    $typeMassage =$_REQUEST['typeMassage'];
    $Massage =$_REQUEST['Massage'];



    $email=$_REQUEST['email'];
  }
  $mail=get_option('admin_email');

  $date=date('d-m-Y');
  $title="Нове повідомлення (".$date.')';
  $body="В вас нове повідомлення від:".$name  . "<br>";



  if (strlen($email)>3)
    $body.="Email: <a href=\"mailto:".$email."\">".$email."</a><br>
  Text: ".$text."<br>";
  $headers = array(
    'Content-Type: text/html; charset=UTF-8',
    "From: ".get_option('siteurl')." <".$mail.">" . "\r\n");
  $sent_message = wp_mail( $mail, $title, $body, $headers );
  if ( $sent_message ) {
    // the message was sent...
    echo 'Повідомлення адміністратору відправлено ('.$name.':'.$text.')';
  } else {
    // the message was not sent...
    var_dump($_REQUEST);
    echo 'Повідомлення адміністратору не відправлено!</br>';
  }
  die();
}

add_action( 'wp_ajax_nopriv_contactMassage', __NAMESPACE__ . '\\MassageSend_message' );
add_action( 'wp_ajax_contactMassage', __NAMESPACE__ . '\\MassageSend_message' );


if( function_exists('acf_add_options_page') ) {

  $languages = array( 'uk', 'ru');
  foreach ( $languages as $lang ) {
    acf_add_options_sub_page( array(
      'page_title' => 'Options (' . strtoupper( $lang ) . ')',
      'menu_title' => __('Options (' . strtoupper( $lang ) . ')', 'lionline'),
      'menu_slug'  => "options-${lang}",
      'post_id'    => $lang,
      'parent'     => $parent['menu_slug']
    ) );
  }
}

// add editor the privilege to edit theme
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );


add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;
  }
  else  {
    $title = post_type_archive_title( '', false );
  }
  return $title;
});
