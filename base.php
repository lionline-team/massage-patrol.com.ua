<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
  <!--[if IE]>
  <div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
  </div>
  <![endif]-->
  <?php
  do_action('get_header');
  get_template_part('templates/header');
  ?>

  <?php include Wrapper\template_path(); ?>
  <div class="reveal" id="exampleModal1" data-reveal="">
      <?php get_template_part('templates/block','PopupForm'); ?>
      <button class="close-button" data-close="" aria-label="Close reveal" type="button"><span aria-hidden="true">×</span></button>
  </div>
  
  <?php

  do_action('get_footer');
  get_template_part('templates/footer');
  wp_footer();
  ?>
</body>
</html>
