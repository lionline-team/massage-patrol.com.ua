 <?php while (have_posts()) : the_post(); ?>
 	<slider>
 		<div class="slider-wrap">
 			<div class="slder-items" id="mainSlider">
 				<?php if( have_rows('slider') ):?>
 					<?php while ( have_rows('slider') ) : ?>
 						<?php the_row(); ?>
 						<div class="slider-item">
 							<div class="slider-item__img"><img src="<?php the_sub_field('slide_image');?>" alt=""></div>
 							<div class="slider-item__text">
 								<div class="row">
 									<div class="large-7">
 										<p><?php the_sub_field('slide_title');?></p>
 									</div>
 								</div>
 							</div>
 							<div class="slider-item__btn">
 								<?php while( have_rows('buttons') ): the_row(); ?>
 									<a class="btn" href="<?php the_sub_field('slide_button_link');?>"><?php the_sub_field('slide_button_text');?></a>
 								<?php endwhile; ?>
 							</div>
 						</div>

 					<?php  endwhile; ?>
 				<?php endif; ?>

 			</div>
 		</div>
 	</slider>
 	<section>

 		<?php get_template_part( 'templates/block', 'massages' ); ?>

 		<section>
 			<div class="advanteges">
 				<div class="row">
 					<div class="title"><span><?php the_field('advantages_title');?></span></div>

 					<div class="advanteges__items">
 						<?php if( have_rows('advantages') ):?>
 							<?php while ( have_rows('advantages') ) : ?>
 								<?php the_row(); ?>

 								<div class="advanteges-item column large-3 medium-3 small-6">
 									<div class="advanteges-item__icon">
 										<img src="<?php the_sub_field('advantages_icon');?>" alt="">
 									</div>
 									<div class="advanteges-item__text"><span><?php the_sub_field('advantages_name');?></span>
 										<p><?php the_sub_field('advantages_text');?></p>
 									</div>
 								</div>
 							<?php  endwhile; ?>
 						<?php endif; ?>
 					</div>
 				</div>
 			</div>
 		</section>

 		<?php get_template_part('templates/block','reviews'); ?>

 		<section>
 			<div class="about-wrap">
 				<div class="row">
 					<div class="title"><span><?php the_field('about_title');?></span></div>
 					<div class="about">

 						<?php if( have_rows('about_home') ):?>
 							<?php while ( have_rows('about_home') ) : ?>
 								<?php the_row(); ?>
 								<div class="about__foto column large-6"><img src="<?php the_sub_field('about_foto');?>" alt=""></div>
 								<div class="about__text column large-6">
 									<?php the_sub_field('about_text');?>
 									<?php if( have_rows('about_buttons') ):?>
 										<?php while ( have_rows('about_buttons') ) : ?>
 											<?php the_row(); ?>
 											<div class="about__btn">

 												<a class="btn btn_transp" href="<?php the_sub_field('button_1_link');?>"><?php the_sub_field('button_1_text');?></a>
 												<a class="btn" href="<?php the_sub_field('button_2_link');?>"><?php the_sub_field('button_2_text');?></a>

 											</div>

 										<?php  endwhile; ?>
 									<?php endif; ?>
 								</div>
 							<?php  endwhile; ?>
 						<?php endif; ?>
 					</div>
 				</div>
 			</div>
 		</section>


 		<section>
 			<div class="row">
 				<div class="events clearfix">
 					<div class="title"><span><?php _e('Our events','lionline');?></span></div>
 					<div class="column large-6 clearfix hide-for-small-only">
 						<a href="<?php echo get_permalink( );?>">
 							<div class="events-next">

 								<?php $first_post = get_posts( array(
 									'numberposts' => 1,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'category',
                      'field'    => 'term_id',
                      'terms'    =>  get_field('events_cat',pll_current_language('slug')),
                    ),
                  ),

                ));
                ?>

                <?php foreach( $first_post as $post ) : setup_postdata( $post ); ?>
                 <div class="events-next__img"><?php the_post_thumbnail( 'pg-main' ); ?></div>
                 <div class="events-next__date"><span><?php echo get_the_date('j') ; ?> </span><span><?php echo get_the_date('M') ; ?></span></div>
                 <div class="events-next__title"><a href="<?php echo get_permalink( );?>"><?php the_title(); ?></a></div>
                 <div class="events-next__text">
                  <p><?php the_excerpt(); ?></p>
                </div>
              <?php endforeach; ?>
            </div>
          </a>
        </div>


        <div class="column large-6 clearfix">
          <?php $args = array(
           'posts_per_page' 	=> 3,
           'offset'			=> 1,
           'tax_query' => array(
            array(
              'taxonomy' => 'category',
              'field'    => 'term_id',
              'terms'    =>  get_field('events_cat',pll_current_language('slug')),
            ),
          ),

         );
         $posts = get_posts( $args ); ?>
         <?php
         add_filter( 'excerpt_length', function(){
          return 25;
        } )
        ;?>
        <div class="events-items">

          <?php
          foreach ( $posts as $post ) : setup_postdata( $post ); ?>
            <div class="events-item">
             <div class="events-item__date hide-for-small-only"><span><?php echo get_the_date('j') ; ?> </span><span><?php echo get_the_date('M') ; ?></span></div>
             <div class="events-text">
              <div class="events-item__title"><a href="<?php echo get_permalink( );?>"><?php the_title();?></a></div>
              <div class="events-item__text">
               <p><?php echo  get_the_excerpt();?></p>
             </div>
           </div>
         </div>

       <?php endforeach;
       wp_reset_postdata();?>
     </div>
   </div>
   <div class="all-events column"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"> <i class="fa fa-chevron-right" aria-hidden="true"></i><?php _e('View all events','lionline');?></a></div>
 </div>
</div>
</section>
<section>
 <div class="courses">
  <div class="row">
   <div class="title"><span><?php the_field('courses_title');?></span></div>
   <div class="courses-content">

    <?php if( have_rows('courses') ):?>
     <?php while ( have_rows('courses') ) : ?>
      <?php the_row(); ?>

      <div class="courses__text column large-6 small-12">

       <div class="courses-text">
        <?php the_sub_field('courses_text');?>
      </div>
      <div class="courses__btn"><a class="btn" href="<?php the_sub_field('courses_button_link');?>"><?php the_sub_field('courses_button_text');?></a></div>
    </div>
    <div class="courses__foto column large-5 small-12">
     <img src="<?php the_sub_field('courses_image');?>" alt="">
   </div>

 <?php  endwhile; ?>
<?php endif; ?>
</div>
</div>
</div>
</section>
<?php get_template_part('templates/block','HomeForm'); ?>

<?php get_template_part('templates/block','contact'); ?>

<?php get_template_part('templates/block','partners'); ?>

<?php endwhile; ?>
