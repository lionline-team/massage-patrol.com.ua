<div class="sidebar_right column large-3">
  <?php get_search_form();?>
  <div class="categories"><span><?php _e('Categories','lionline');?></span>
    <ul>
      <?php
      $args = array(
        'title_li'    => "",
        'exclude'     => array(1,14 ),
        'hide_empty'  => true
      );
      wp_list_categories($args) ;?>
    </ul>
  </div>
</div>
