 <div class="row column">
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
          <li><a href="#">Головна </a></li>
          <li><a href="#">Типи Масажу </a></li>
          <li><span class="show-for-sr">Current: </span>Класичний з маслом</li>
        </ul>
      </nav>
    </div>
    <div class="row">
      <div class="contactPage">
        <div class="title"><span>Контакти</span></div>
        <div class="bases column">
          <div class="bases-title"><span>Бази</span></div>
          <div class="bases-items clearfix">
            <div class="bases-item large-3 column medium-6">
              <div class="bases-item__contact">
                <div class="bases-item__city"><i class="fa fa-map-marker" aria-hidden="true"></i><span>м. Київ</span></div>
                <div class="bases-item__address">
                  <p>Адреса:</p><a class="marker-link" href="#" data-markerid="0">вул. Набережно-Луговая 3 </a>
                </div>
              </div>
              <div class="bases-item__work-time">
                <div class="work-time">
                  <p>Час роботи:</p>
                  <ul>
                    <li>Пн-Пт 9-18</li>
                    <li>Сб-Нд 10-16</li>
                  </ul>
                </div>
                <div class="bases-item__phone">
                  <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
                </div>
              </div>
              <div class="bases-item__btn"><a href="#">Записатись<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
            </div>
            <div class="bases-item large-3 column medium-6">
              <div class="bases-item__contact">
                <div class="bases-item__city"><i class="fa fa-map-marker" aria-hidden="true"></i><span>м. Київ</span></div>
                <div class="bases-item__address">
                  <p>Адреса:</p><a class="marker-link" href="#" data-markerid="1">вул. Набережно-Луговая 3 </a>
                </div>
              </div>
              <div class="bases-item__work-time">
                <div class="work-time">
                  <p>Час роботи:</p>
                  <ul>
                    <li>Пн-Пт 9-18</li>
                    <li>Сб-Нд 10-16</li>
                  </ul>
                </div>
                <div class="bases-item__phone">
                  <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
                </div>
              </div>
              <div class="bases-item__btn"><a href="#">Записатись<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
            </div>
            <div class="bases-item large-3 column medium-6">
              <div class="bases-item__contact">
                <div class="bases-item__city"><i class="fa fa-map-marker" aria-hidden="true"></i><span>м. Київ</span></div>
                <div class="bases-item__address">
                  <p>Адреса:</p><a class="marker-link" href="#" data-markerid="1">вул. Набережно-Луговая 3 </a>
                </div>
              </div>
              <div class="bases-item__work-time">
                <div class="work-time">
                  <p>Час роботи:</p>
                  <ul>
                    <li>Пн-Пт 9-18</li>
                    <li>Сб-Нд 10-16</li>
                  </ul>
                </div>
                <div class="bases-item__phone">
                  <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
                </div>
              </div>
              <div class="bases-item__btn"><a href="#">Записатись<i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
            </div>
          </div>
        </div>
        <div class="partners">
          <div class="partners-title"><span>Партнери</span></div>
          <div class="partners-items">
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="2">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="3">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="4">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="5">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="6">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
            <div class="partners-item column large-3 medium-4">
              <div class="partners-item__name"><a href="#">Спорткулб “BANDA”</a></div>
              <div class="partners-item__address">
                <p>Адреса:</p><a class="marker-link" href="#" data-markerid="6">вул. Набережно-Луговая 3</a>
              </div>
              <div class="partners-item__phone">
                <p>Телефон:</p><a href="#">(+38) 066 475 7299</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="map clearfix">
      <div id="markers"></div>
      <div id="map-canvas">
       <script>
            
              function initialize() {

            var markers = new Array();
          
            var mapOptions = {
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: new google.maps.LatLng(50.450657,  30.522930),
           
            };
            
          
            var locations = [
                [new google.maps.LatLng(50.450587, 30.515987),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],
                
                [new google.maps.LatLng(50.450667, 30.510312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],

                [new google.maps.LatLng(50.460667, 30.530312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],

                [new google.maps.LatLng(50.440667, 30.520312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],

                [new google.maps.LatLng(50.430667, 30.530312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],

                [new google.maps.LatLng(50.450667, 30.530312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],

                [new google.maps.LatLng(50.460667, 30.500312),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg']
            ];



            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            var infowindow = new google.maps.InfoWindow();

            for (var i = 0; i < locations.length; i++) {

                // Append a link to the markers DIV for each marker
                // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

                var marker = new google.maps.Marker({
                    position: locations[i][0],
                    map: map,
                    icon:locations[i][2],
                    title: locations[i][1],
                });

                // Register a click event listener on the marker to display the corresponding infowindow content
                google.maps.event.addListener(marker, 'click', (function (marker, i) {

                    return function () {
                        infowindow.setContent(locations[i][1]);
                        infowindow.open(map, marker);
                    };

                })(marker, i));

                // Add marker to markers array
                markers.push(marker);
            }

            // Trigger a click event on each marker when the corresponding marker link is clicked
            jQuery('.marker-link').on('click', function (e) {
                e.preventDefault();
                google.maps.event.trigger(markers[jQuery(this).data('markerid')], 'click');
            });
        }

          </script>

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initialize"></script>
      </div>
    </div>
    <div class="row">
      <div class="contact-form column large-6 large-offset-3">
        <form>
          <div class="title"><span>У вас є питання? </span>
            <div class="sub-title"><span>Відповімо вам якнайшвидше</span></div>
          </div>
          <p>Ім’я*</p>
          <input type="text">
          <p>Імейл</p>
          <input type="email">
          <p>Повідомлення</p>
          <textarea></textarea>
          <div class="contact-form__btn">
            <button class="btn btn_yellow button success" type="submit">Записатись</button>
          </div>
        </form>
      </div>
    </div>