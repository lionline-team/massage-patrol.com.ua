<div class="row">
  <div class="_404 column large-8 large-offset-2">
    <div class="_404__title"><span>Помилка 404</span></div>
    <div class="_404__text">
      <p>Послуга або сторінка не знайдені </p><span>Почніть з головної сторінки!</span>
    </div>
    <div class="_404__btn"><a class="btn" href="<?php echo home_url( '/' ); ?>">На головну</a></div>
  </div>
</div>
