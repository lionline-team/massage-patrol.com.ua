<?php
/**
 * Template Name: contacts Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
 <div class="row column">
  <nav aria-label="You are here:" role="navigation">
    <ul class="breadcrumbs">
     <?php yoast_breadcrumb( '<li>','</li>' ); ?>
     <!--  <li><a href="#">Головна </a></li>
      <li><a href="#">Типи Масажу </a></li>
      <li><span class="show-for-sr">Current: </span>Класичний з маслом</li> -->
    </ul>
  </nav>
</div>
<div class="row">
  <div class="contactPage">
    <div class="title"><span><?php the_title() ?></span></div>
    <div class="bases column">
      <div class="bases-title"><span><?php _e('bases','lionline');?></span></div>
      <div class="bases-items clearfix">

        <?php if( have_rows('bases', pll_current_language('slug')) ):?>
          <?php $id=0; ?>

          <?php while ( have_rows('bases', pll_current_language('slug')) ) : ?>
            <?php the_row(); ?>

            <div class="bases-item large-3 column medium-6">
              <div class="bases-item__contact">
                <div class="bases-item__city">
                  <a class="marker-link_base" href="#map-canvas" data-markerid="<?php echo $id ;?>">
                    <i class="fas fa-map-marker-alt"></i>
                    <span><?php the_sub_field('city');?></span>
                  </a>
                </div>
                <div class="bases-item__address">
                  <p><?php _e('contacts adress','lionline');?></p>
                  <a class="marker-link_base" href="#map-canvas" data-markerid="<?php echo $id ;?>"><?php the_sub_field('address');?></a>
                </div>
              </div>
              <div class="bases-item__work-time">
                <div class="work-time">
                  <p><?php _e('contacts work-time','lionline');?></p>
                  <?php
                  $field=get_sub_field('work_time');
                  $lines = explode("\n", $field);
                  if ( !empty($lines) ) {
                    echo '<ul>';
                    foreach ( $lines as $line ) {
                      echo '<li>'. trim( $line ) .'</li>';
                    }
                    echo '</ul>';
                  }
                  ?>
                </div>
                <div class="bases-item__phone">
                  <p>Телефон:</p><a href="tel:<?php the_sub_field('bases_phone');?>"><?php the_sub_field('bases_phone');?></a>
                </div>
              </div>
              <div class="bases-item__btn">
                <a href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>


          <?php $id++;?>
        <?php  endwhile; ?>
      <?php endif; ?>

    </div>
  </div>
  <div class="partners">
    <div class="partners-title"><span><?php _e('partners','lionline');?></span></div>
    <div class="partners-items">

      <?php if( have_rows('partners_base', pll_current_language('slug')) ):?>
        <?php $id=0; ?>

        <?php while ( have_rows('partners_base', pll_current_language('slug')) ) : ?>
          <?php the_row(); ?>

          <div class="partners-item column large-3 medium-4">
            <div class="partners-item__name">
              <a  class="marker-link_partner" href="#map-canvas" data-markerid="<?php echo $id ;?>">
                <i class="fas fa-map-marker-alt"></i>
                <span><?php the_sub_field('partners_name');?></span>
              </a>
            </div>
            <div class="partners-item__address">
              <p><?php _e('contacts adress','lionline');?></p><a class="marker-link_partner" href="#map-canvas" data-markerid="<?php echo $id ;?>"><?php the_sub_field('address');?></a>
            </div>
            <div class="partners-item__phone">
              <p>Телефон:</p><a href="tel:<?php the_sub_field('bases_phone');?>"><?php the_sub_field('bases_phone');?></a>
            </div>
            <div class="bases-item__btn">
              <a href="<?php the_sub_field('link');?>"><?php the_sub_field('link_text');?>
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>

        <?php $id++;?>
      <?php  endwhile; ?>
    <?php endif; ?>
  </div>
</div>
</div>
</div>
<div class="map clearfix">
  <div id="markers"></div>
  <div id="map-canvas">
   <script>

    function initialize() {

      var markers = new Array();
      var markers_partner = new Array();

      var mapOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(50.450657,  30.522930),

      };


      var locations = [


      <?php if( have_rows('bases', pll_current_language('slug')) ):?>
        <?php $id=0; ?>
        <?php while ( have_rows('bases', pll_current_language('slug')) ) : ?>
          <?php the_row(); ?>

          <?php
          $items=get_sub_field('coordinates');
          ?>
          [new google.maps.LatLng(<?php echo $items['lat'] ?>, <?php echo $items['lng'] ?>),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span><?php the_sub_field('city');?></span><p><?php echo str_ireplace(array("\r","\n",'\r','\n'),'',get_sub_field('address'));?></p><a href="#"><?php the_sub_field('bases_phone');?></a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],


          <?php $id++;?>
        <?php  endwhile; ?>
      <?php endif; ?>

      ];


      var locations_partner = [
      <?php if( have_rows('partners_base', pll_current_language('slug')) ):?>
        <?php $id=0; ?>
        <?php while ( have_rows('partners_base', pll_current_language('slug')) ) : ?>
          <?php the_row(); ?>

          <?php
          $items=get_sub_field('coordinates_part');
          ?>
          [new google.maps.LatLng(<?php echo $items['lat'] ?>, <?php echo $items['lng'] ?>),'<div class="info_block"><span><?php the_sub_field('partners_name');?></span><p><?php echo str_ireplace(array("\r","\n",'\r','\n'),'',get_sub_field('address'));?></p><a href="#"><?php the_sub_field('bases_phone');?></a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],


          <?php $id++;?>
        <?php  endwhile; ?>
      <?php endif; ?>


      ];

      console.log(locations_partner);
      console.log(locations);


      var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var infowindow = new google.maps.InfoWindow();

      for (var i = 0; i < locations_partner.length; i++) {

        // Append a link to the markers DIV for each marker
        // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

        var marker = new google.maps.Marker({
          position: locations_partner[i][0],
          map: map,
          icon:locations_partner[i][2],
          title: locations_partner[i][1],
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            infowindow.setContent(locations_partner[i][1]);
            infowindow.open(map, marker);
          };

        })(marker, i));

        // Add marker to markers array
        markers_partner.push(marker);
      }

      for (var i = 0; i < locations.length; i++) {

        // Append a link to the markers DIV for each marker
        // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

        var marker = new google.maps.Marker({
          position: locations[i][0],
          map: map,
          icon:locations[i][2],
          title: locations[i][1],
        });

        // Register a click event listener on the marker to display the corresponding infowindow content
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

          return function () {
            infowindow.setContent(locations[i][1]);
            infowindow.open(map, marker);
          };

        })(marker, i));

        // Add marker to markers array
        markers.push(marker);
      }

      // Trigger a click event on each marker when the corresponding marker link is clicked
      jQuery('.marker-link_base').on('click', function (e) {
        e.preventDefault();
          var top = jQuery("#map-canvas").offset().top-50;
          jQuery('body,html').animate({scrollTop: top}, 1000);
        google.maps.event.trigger(markers[jQuery(this).data('markerid')], 'click');
      });
      jQuery('.marker-link_partner').on('click', function (e) {
        e.preventDefault();
          var top = jQuery("#map-canvas").offset().top-50;
          jQuery('body,html').animate({scrollTop: top}, 1000);
        google.maps.event.trigger(markers_partner[jQuery(this).data('markerid')], 'click');
      });
    }

  </script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initialize"></script>
</div>
</div>

<?php get_template_part('templates/block','ContactForm'); ?>

<?php endwhile; ?>
