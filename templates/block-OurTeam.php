<section>
  <div class="our-team">
    <div class="row">
      <div class="title"><span><?php the_field('our_team_title', pll_current_language('slug'));?></span></div>
      <div class="our-team__slider-wrap">
        <div class="our-team__slier">

          <?php if( have_rows('our_team', pll_current_language('slug')) ):?>
            <?php while ( have_rows('our_team', pll_current_language('slug')) ) : ?>
              <?php the_row(); ?>
              <div class="large-3 medium-4 small-6 column">
                <div class="our-team-item">
                <div class="our-team-item__foto">
                  <img src="<?php the_sub_field('foto', pll_current_language('slug'));?>" alt="">
                </div>
                <div class="our-team-item__info">
                  <div class="item-name">
                   <?php if  (get_sub_field('interview', pll_current_language('slug'))) : ?>
                    <a href="<?php the_sub_field('interview', pll_current_language('slug'));?>">
                     <span><?php the_sub_field('name', pll_current_language('slug'));?></span></div>
                   </a>
                   <?php else : ?>
                     <span><?php the_sub_field('name', pll_current_language('slug'));?></span></div>
                   <?php endif; ?>

                   <div class="item-info">
                    <p><?php the_sub_field('info', pll_current_language('slug'));?></p>

                    <?php if  (get_sub_field('interview', pll_current_language('slug'))) : ?>
                      <a href="<?php the_sub_field('interview', pll_current_language('slug'));?>">
                        <?php _e("Інтерв'ю",'lionline');?>
                      </a>
                    <?php endif; ?>

                  </div>
                  <div class="item-info_soc">
                    <?php if  (get_sub_field('facebook', pll_current_language('slug'))) : ?>
                      <a href="<?php the_sub_field('facebook', pll_current_language('slug'));?>">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                      </a>
                    <?php endif; ?>
                    <?php if  (get_sub_field('instagram', pll_current_language('slug'))) : ?>
                      <a href="<?php the_sub_field('instagram', pll_current_language('slug'));?>">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                      </a>
                    <?php endif; ?>
                    <?php if  (get_sub_field('link', pll_current_language('slug'))) : ?>
                      <a href="<?php the_sub_field('instagram', pll_current_language('slug'));?>">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                      </a>
                    <?php endif; ?>

                  </div>
                </div>
              </div>
              </div>


            <?php  endwhile; ?>
          <?php endif; ?>

        </div>
        <!-- <div class="t-Next"><img src="<?php echo get_template_directory_uri();?>/dist/images/sl_next.svg" alt=""></div>
        <div class="t-Prev"><img src="<?php echo get_template_directory_uri();?>/dist/images/sl_prev.svg" alt=""></div> -->
      </div>
      <div class="our-team__text">
        <p><?php the_field('our_team_text', pll_current_language('slug'));?></p>
      </div>
      <div class="our-team__btn"><a class="btn btn_yellow" href="<?php the_field('our_team_btn_link', pll_current_language('slug'));?>"><?php the_field('our_team_btn_text', pll_current_language('slug'));?></a></div>
    </div>
  </div>
</section>
