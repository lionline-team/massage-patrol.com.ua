/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        $(document).foundation(); // Foundation JavaScript

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.





// ====================== smooth scroll anchor ================

  // jQuery(function() {
  //   jQuery('.scrolldown').click(function() {
  //     if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
  //       var target = jQuery(this.hash);
  //       target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
  //       if (target.length) {
  //         jQuery('html,body').animate({
  //           scrollTop: target.offset().top - 80
  //         }, 1000);
  //         return false;
  //       }
  //     }
  //   });
  // });

// ====================== end smooth scroll anchor ================








// function initialize() {

//     var markers = new Array();

//     var mapOptions = {
//         zoom: 13,
//         mapTypeId: google.maps.MapTypeId.ROADMAP,
//         center: new google.maps.LatLng(50.450657,  30.522930),

//     };


//     var locations = [
//         [new google.maps.LatLng(50.450587, 30.515987),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.450667, 30.510312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.460667, 30.530312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.440667, 30.520312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.430667, 30.530312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.450667, 30.530312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg'],

//         [new google.maps.LatLng(50.460667, 30.500312),'<div class="info_block"><div class="logo"><img src="./dist/images/logo.png"></div><span>м.Київ</span><p>вул.Набережно-Луговая 3</p><a href="#">80 (99) 999 99 99</a></div>','./dist/images/map_marker.svg']
//     ];



//     var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

//     var infowindow = new google.maps.InfoWindow();

//     for (var i = 0; i < locations.length; i++) {

//         // Append a link to the markers DIV for each marker
//         // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

//         var marker = new google.maps.Marker({
//             position: locations[i][0],
//             map: map,
//             icon:locations[i][2],
//             title: locations[i][1],
//         });

//         // Register a click event listener on the marker to display the corresponding infowindow content
//         google.maps.event.addListener(marker, 'click', (function (marker, i) {

//             return function () {
//                 infowindow.setContent(locations[i][1]);
//                 infowindow.open(map, marker);
//             };

//         })(marker, i));

//         // Add marker to markers array
//         markers.push(marker);
//     }

//     // Trigger a click event on each marker when the corresponding marker link is clicked
//     jQuery('.marker-link').on('click', function (e) {
//         e.preventDefault();
//         google.maps.event.trigger(markers[jQuery(this).data('markerid')], 'click');
//     });
// }



jQuery(document).ready(function(){      // global document.ready

function autoHeightAnimate(element) {
  var curHeight = element.height(), // Get Default Height
  autoHeight = element.css('height', 'auto').height(); // Get Auto Height
  element.height(curHeight); // Reset to Default Height
  element.stop().animate({
    height: autoHeight,
  }); // Animate to Auto Height
}

jQuery('#allForm ').click(function(){
  jQuery(this).hide();
   // autoHeightAnimate(jQuery(".form "));
  jQuery('.form').addClass('active');
  jQuery('#btn_resset').fadeIn();
  jQuery('.form_hide').fadeIn();
});

jQuery("#btn_resset").click(function(){
   jQuery('.form').removeClass('active');
   // jQuery('.form').stop().animate({
   //    height: 220,
   //  });

   jQuery(this).hide();
   jQuery('#allForm ').show();
});



              // var animateButton = function(e) {

              //   e.preventDefault();
              //   //reset animation
              //   e.target.classList.remove('animate');

              //   e.target.classList.add('animate');

              //   e.target.classList.add('animate');
              //   setTimeout(function(){
              //     e.target.classList.remove('animate');
              //   },4000);
              // };

              // var classname = document.getElementsByClassName("button");

              // for (var i = 0; i < classname.length; i++) {
              //   classname[i].addEventListener('click', animateButton, false);
              // }

              // jQuery('.button').click(function(e){
              //   e.preventDefault();
              //   // jQuery(this).removeClass('animate');
              //   jQuery(this).addClass('animate');
              //   setTimeout(function(){
              //     jQuery('#popup').trigger("reset");
              //   },500);
              //   setTimeout(function(){
              //    jQuery(this).removeClass('animate');
              //   },4000);

              // });








// jQuery(".button").click(function(e){
//   e.preventDefault();
//   jQuery(this).addClass('animate')
// })
//////////////    partners slider  /////////////////////////////

  jQuery(".partners__slider").slick({
    slidesToShow: 6,
    centerMode: true,
    centerPadding: '90px',
    // variableWidth: true,
    arrows:false,
    speed:1500,
    autoplay:true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          centerPadding: '60px',
          slidesToShow: 4
        }
      },
      {
        breakpoint: 640,
        settings: {
          centerPadding: '40px',
          slidesToShow: 2
        }
      }
    ]
  });


/////////////////////////////////////////////////////////////////

jQuery(".info-icon").click(function(){
  // jQuery(".price-item__info").hide(200)
  jQuery(this).next().show(200);
})

jQuery(".info__close").click(function(){
  jQuery(this).parent().hide(200);
})

jQuery(document).mouseup(function (e) {
  var container = jQuery(".price-item__info");
  if (container.has(e.target).length === 0){
    container.hide(200);

  }
});



/////////////////////////////////////////////////////////////////





  jQuery('[data-triger]').click(function(){
    var collection =  jQuery(this).data('collection');
    var triger = jQuery(this).data('triger');
    jQuery('[data-collection="'+collection+'"][data-triger]').removeClass('tab-active');
    jQuery(this).addClass('tab-active');
    jQuery('[data-collection="'+collection+'"]').removeClass('active');
    jQuery('[data-collection="'+collection+'"][data-target="'+triger+'"]').addClass('active');

  });


///////////////////  lang Select   //////////////////////////////

    jQuery("#langSelect").niceSelect();
    jQuery("#formAdrress").niceSelect();
    jQuery("#typeMassage").niceSelect();
    jQuery("#formTime").niceSelect();
    jQuery("#duration").niceSelect();

///////////////// MAIN SLIDER ////////////////////

  jQuery("#mainSlider").slick({
    slidesToShow: 1,
    arrows:false,
    speed:1500,
    autoplay:true,
    dots:true,
    fade:true,
    dotsClass: 'slick-control',
  });


   jQuery(".reviews-items").slick({
    slidesToShow: 1,
    speed:500,
    nextArrow: jQuery('.r-Next'),
    prevArrow: jQuery('.r-Prev'),
    adaptiveHeight: true,
    // autoplay:true,
  });

  // jQuery('.our-team__slier').slick({
  //   slidesToShow: 4,
  //   nextArrow: jQuery('.t-Next'),
  //   prevArrow: jQuery('.t-Prev'),
  //    responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         centerPadding: '60px',
  //         slidesToShow: 2
  //       }
  //     },
  //     {
  //       breakpoint: 640,
  //       settings: {
  //         centerPadding: '40px',
  //         slidesToShow: 1
  //       }
  //     }
  //   ]
  // });
//////////////////////////////////////////////////

  jQuery( ".icon" ).click(function(){
    jQuery(this).toggleClass('active');
    jQuery('.header__menu').toggleClass('active');
    jQuery('body').toggleClass('hiddenScroll');
  });

   jQuery( ".icon_contact" ).click(function(){
    jQuery(this).toggleClass('active');
    jQuery('.header-top').toggleClass('active');
    jQuery('body').toggleClass('hiddenScroll');
  });

  jQuery(document).mouseup(function (e) {
    var container = jQuery(".header__menu");
    if (container.has(e.target).length === 0){
      container.removeClass('active');
      jQuery( ".icon" ).removeClass('active');
      jQuery('body').removeClass('hiddenScroll');
    }
  });

  jQuery(document).mouseup(function (e) {
    var container = jQuery(".header-top");
    if (container.has(e.target).length === 0){
      container.removeClass('active');
      jQuery( ".icon_contact" ).removeClass('active');
      jQuery('body').removeClass('hiddenScroll');
    }
  });

  jQuery('[data-opener]').click(function(e){
    e.stopPropagation();
    e.preventDefault();
    jQuery('#h-search').focus();
    jQuery('[data-opener]').removeClass('is-active');
    jQuery('[data-opener-target]').removeClass('is-active');
    jQuery(this).addClass('is-active');
    jQuery('[data-opener-target="'+jQuery(this).data('opener')+'"]').addClass('is-active');
  });


  jQuery(document).mouseup(function (e) {
    var container = jQuery(".search");
    if (container.has(e.target).length === 0){
      container.removeClass('is-active');
    }
  });

  // $(window).click(function() {
  //   $('[data-opener]').removeClass('is-active');
  //   $('[data-opener-target]').removeClass('is-active');
  // });

  jQuery.fn.fdatepicker.dates['uk'] = {
    days: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"],
    daysShort: ["Нед", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Нед"],
    daysMin: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Нд"],
    months: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
    monthsShort: ["Січ", "Лют", "Бер", "Кві", "Тра", "Чер", "Лип", "Сер", "Вер", "Жов", "Лис", "Гру"],
    today: "Сьогодні"
  };

  var nowTemp = new Date();
  jQuery("#dp2").fdatepicker({
    initialDate: nowTemp,
    format: 'mm.dd.yyyy',
    disableDblClickSelection: true,
    language: 'uk',
    startDate: nowTemp

  });

  // if (jQuery(window).width()<640) {
  //   jQuery("#dp2").prop('readonly',true);
  // }

 jQuery(window).scroll(function() {
  if (jQuery(this).scrollTop() > 49){
    jQuery("header").addClass('fixed');
  }
  else  {
    jQuery("header").removeClass('fixed');
  }

});

/////////////////////  timer  ////////////////////////

    jQuery("#countdown").countdown("2018/10/12", function(event) {
      var  br =  '<br/>';
      jQuery("#countdown").html(event.strftime('%D днів %H годин '+br+' %M хвилин %S секунд'));

    });
//////////////////////////////////////////////////////
});

jQuery(document).ready(function(){
 // wordpress admin panel
    jQuery('html').attr('style', 'margin-top: 0!important');
    jQuery('#wpadminbar').addClass('overflow');
    var hide;
    jQuery('#wpadminbar').on('mouseover', function(){
      setTimeout(function(){
        jQuery('#wpadminbar').removeClass('overflow');
      },1000);
      if(!jQuery('#wpadminbar').hasClass('open')){
        jQuery('#wpadminbar').addClass('open overflow');
      } else{
        clearTimeout(hide);
      }
    });
    jQuery('#wpadminbar').on('mouseleave', function(){
      hide = setTimeout(function(){
        jQuery('#wpadminbar').addClass('overflow');
        jQuery('#wpadminbar').removeClass('open');
      },2000);
    });
  // end wordpress admin panel
});






