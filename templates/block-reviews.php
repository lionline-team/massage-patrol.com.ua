<section>
 		<div class="reviews">
 			<div class="row">
 				<div class="reviews-slider">
 					<div class="title"><span><?php the_field('reviews_title', pll_current_language('slug'));?></span>
 						<div class="sub-title"><span><?php the_field('reviews_sub_title', pll_current_language('slug'));?></span></div>
 					</div>
 					<div class="reviews-items">

 						<?php if( have_rows('reviews', pll_current_language('slug')) ):?>
 							<?php while ( have_rows('reviews', pll_current_language('slug')) ) : ?>
 								<?php the_row(); ?>


 								<div class="reviews-item">
 									<div class="reviews-item__text">
 										<p><?php the_sub_field('reviews_text', pll_current_language('slug'));?></p>
 									</div>
 									<div class="reviews-item__user">
 										<div class="reviews-user_foto">
 											<img src="<?php the_sub_field('user_foto', pll_current_language('slug'));?>" alt="">
 										</div>
 										<div class="reviews-user_name">
 											<span><?php the_sub_field('user_name', pll_current_language('slug'));?></span>
 											<div class="reviews-user_status">
 												<span><?php the_sub_field('user_status', pll_current_language('slug'));?></span>
 											</div>
 										</div>
 									</div>
 								</div>


 							<?php  endwhile; ?>
 						<?php endif; ?>

 					</div>
 					<div class="r-Next"><img src="<?php echo get_template_directory_uri();?>/dist/images/sl_next.svg" alt=""></div>
 					<div class="r-Prev"><img src="<?php echo get_template_directory_uri();?>/dist/images/sl_prev.svg" alt=""></div>
          <?php $reviews_button=get_field('reviews_link', pll_current_language('slug'));?>
 					<div class="all-reviews"><a target="<?= $reviews_button['target'];?>" href="<?= $reviews_button['url'];?>"> <i class="fa fa-chevron-right" aria-hidden="true"></i><?= $reviews_button['title'];?></a></div>
 				</div>
 			</div>
 		</div>
 	</section>
