<?php
/**
 * Template Name: servises Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<div class="row column">
      <nav aria-label="You are here:" role="navigation">
        <ul class="breadcrumbs">
          <li><a href="#">Головна </a></li>
          <li><a href="#">Типи Масажу </a></li>
          <li><span class="show-for-sr">Current: </span>Класичний з маслом</li>
        </ul>
      </nav>
    </div>
    <div class="main">
      <div class="row">
        <div class="sidebar-left column large-3">
          <ul>
            <li class="active"><a href="#">Класичний з маслом</a></li>
            <li><a href="#">Тайський</a></li>
            <li><a href="#">Парний</a></li>
            <li><a href="#">Антицелюлітний</a></li>
          </ul>
        </div>
        <div class="content column large-8">
          <div class="category active" data-collection="1" data-target="0">
            <div class="category__title"><span>Класичний з маслом-1</span></div>
            <div class="category__img"><img src="<?php echo get_template_directory_uri();?>/dist/images/event_foto.png" alt=""></div>
            <div class="category__text">
              <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного аппарата, активизирует образование синовиальной жидкости, улучшает циркуляцию крови, отток лимфы.</p>
            </div>
            <div class="price">
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="categoryEntry.html">общий масаж тела (сторінка категорії)</a></div>
                  <div class="price-item__text column large-6 medium-7">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large-4 medium-5">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="#">общий масаж тела</a></div>
                  <div class="price-item__text column large-6 medium-8">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large-4 medium-4">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="#">общий масаж тела</a></div>
                  <div class="price-item__text column large-6 medium-8">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large-4 medium-4">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="#">общий масаж тела</a></div>
                  <div class="price-item__text column large-6 medium-8">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large4 medium-4">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="#">общий масаж тела</a></div>
                  <div class="price-item__text column large-6 medium-8">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large-4 medium-4">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
              <article>
                <div class="price-item clearfix">
                  <div class="price-item__title"><a href="#">общий масаж тела</a></div>
                  <div class="price-item__text column large-6 medium-8">
                    <p>Общий массаж благотворно действует на весь организм, так прорабатывается все тело — от макушки головы до пальцев на ногах, повышает тонус мышц, суставно-связочного а овышает тонус мышц, суставно-связоч..</p>
                  </div>
                  <div class="price-item__price column large-4 medium-4">
                    <ul>
                      <li><span class="left">70 минут сесия</span><span class="right">570 грн.</span></li>
                      <li><span class="left">100 минут сесия</span><span class="right">750 грн.</span></li>
                      <li><span class="left">130 минут сесия</span><span class="right">1100 грн.</span></li>
                    </ul>
                    <div class="price-item__btn"><a class="btn btn_transp" href="#">Вибрати курс</a></div>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php endwhile; ?>
