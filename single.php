<?php while (have_posts()) : the_post(); ?>
  <div class="row column">
    <nav aria-label="You are here:" role="navigation">
      <ul class="breadcrumbs">
        <?php yoast_breadcrumb( '<li>','</li>' ); ?>
      </ul>
    </nav>
  </div>

  <div class="row">
    <div class="content-block content blog-content column large-9">
      <div class="blog-item large-11">
        <div class="blog-item__date"><span><?php echo get_the_date('j') ; ?> </span><span><?php echo get_the_date('M') ; ?></span></div>
        <div class="blog-item__content">
          <div class="blog-item__title"><?php the_title();?></div>
          <div class="posted-by">
            <span>
              <?php the_category(' ', 'multiple') ;?>
            </span>
          </div>
          <?php the_content();?>
        </div>
      </div>
    </div>
    <?php get_template_part( 'templates/sidebar','' ); ?>
  </div>
<?php endwhile; ?>
