<!-- <?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
<?php endwhile; ?>

<?php the_posts_navigation(); ?> -->


<div class="row column">
	<nav aria-label="You are here:" role="navigation">
		<ul class="breadcrumbs">
			<?php yoast_breadcrumb( '<li>','</li>' ); ?>         
      </ul>
  </nav>
</div>
<div class="row">
	<div class="blog-content column large-9">


		<?php while (have_posts()) : the_post(); ?>
			<article>
				<div class="blog-item large-11">
					<div class="blog-item__date"><span><?php echo get_the_date('j') ; ?> </span><span><?php echo get_the_date('M') ; ?></span></div>
					<div class="blog-item__content">
						<div class="blog-item__foto"> <?php the_post_thumbnail('medium'); ?></div>
						<div class="posted-by"><span>
							Від <a href="#"><?php the_author(); ?> </a></span><span>в <?php the_category(' > ', 'multiple') ;?></span></div>
							<div class="blog-item__title"><a href="<?php echo get_permalink( );?>"><?php the_title();?></a></div>
							<div class="blog-item__text">
								<p><?php echo get_the_excerpt();?> <a href="<?php echo get_permalink( );?>"><?php _e('Read more','lionline');?></a></p>
							</div>
						</div>
					</div>
					<article>
						<!-- <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?> -->
					<?php endwhile; ?>


					
					
					<div class="nav-links">
						<?php wp_pagenavi(); ?>

						<!-- <a class="prev page-numbers" href="#">< назад</a><span class="page-numbers current" aria-current="page">1</span><a class="page-numbers" href="#">2</a><a class="next page-numbers" href="#">Далі > </a> -->
					</div>
				</div>
				
				<div class="sidebar_right column large-3">
					<?php get_search_form();?>
					
					<div class="categories"><span><?php _e('Categories','lionline');?></span>
						<ul>
							<?php 
							$args = array(
								
								'title_li'  => "",
								
								);

								wp_list_categories($args) ;?>
							</ul>
							
							
						</div>
					</div>
				</div>


