
<footer>
  <div class="footer">
    <div class="row">
      <div class="footer__logo column large-3 medium-4 small-12"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png" alt=""/></a></div>
      <div class="footer__menu column large-3 medium-4">
        <p><?php _e('Quick links','lionline');?></p>
        <ul>

           <?php if (has_nav_menu('footer_navi')) :?>
              <?php wp_nav_menu(['theme_location' => 'footer_navi', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s', 'walker' => new Roots\Sage\Extras\Foundation_Nav_Menu()]);?>
            <?php endif;?>

          <!-- <li><a href="#">Домашня</a></li>
          <li><a href="#">Новини</a></li>
          <li><a href="#">Про нас</a></li>
          <li><a href="#">Зв’язок</a></li>
          <li><a href="#">Курси</a></li> -->
        </ul>
      </div>
      <div class="footer__news column large-4 show-for-large">

        <p><?php _e('Latest news','lionline');?></p>
               <?php $args = array(
                    'posts_per_page'  => 4,
                    'offset'      => 0,
                  );
                  $posts = get_posts( $args ); ?>
               <?php
              foreach ( $posts as $post ) : setup_postdata( $post ); ?>

                <ul>
                    <li>
                    <a href="<?php echo get_permalink( );?>"><?php the_title();?></a>
                    <i class="fas fa-calendar-alt"></i>
                    <span><?php echo get_the_date('j F Y') ; ?> </span>
                   </li>
                </ul>



              <?php endforeach;
            wp_reset_postdata();?>





      </div>
      <div class="footer_contact column medium-4 hide-for-large"><span><?php _e('footer contacts','lionline');?></span>
        <ul>
          <li><a href="tel:<?php the_field('contact_phone','option');?>"> <i class="fa fa-phone" aria-hidden="true"></i><?php the_field('contact_phone','option');?></a></li>
          <li><a href="#"> <i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('contact_email','option');?></a></li>
          <li>
              <a href="<?php the_field('contacts_facebook_link','option');?>"><i class="fab fa-facebook-f"></i></a>
              <a href="<?php the_field('contacts_instagram_link','option');?>"><i class="fab fa-instagram"></i></a>
          </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="footer__bottom">
        <div class="footer_copy">
          <sapn><?php _e('footer copy','lionline');?> <a href="#">LIONLINE</a></sapn>
        </div>
        <div class="footer_contact_bottom show-for-large"><span><?php _e('footer contacts','lionline');?></span>
          <ul>
            <li><a href="tel:<?php the_field('contact_phone','option');?>"> <i class="fa fa-phone" aria-hidden="true"></i><?php the_field('contact_phone','option');?></a></li>
            <li><a class="hide-for-small-only" href="#"> <i class="fa fa-envelope" aria-hidden="true"></i><?php the_field('contact_email','option');?></a></li>
            <li><a href="<?php the_field('contacts_facebook_link','option');?>"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="<?php the_field('contacts_instagram_link','option');?>"><i class="fab fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>




<!-- <footer class="content-info">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
  </div>
</footer>
 -->
