<section>
	<div class="row">
		<div class="specialization">
			<div class="specialization__title"><span><?php the_field('specialization_title', pll_current_language('slug'));?></span></div>
			<div class="specialization-items">

				<?php if( have_rows('specialization', pll_current_language('slug')) ):?>
					<?php while ( have_rows('specialization', pll_current_language('slug')) ) : ?>
						<?php the_row(); ?>


						<article>
							<div class="specialization-item">
								<div class="specialization-item__icon"><img src="<?php the_sub_field('specialization_icon', pll_current_language('slug'));?>" alt=""></div>
								<div class="specialization-item__text"><span><?php the_sub_field('specialization_text', pll_current_language('slug'));?></span></div>
							</div>
						</article>

					<?php  endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>