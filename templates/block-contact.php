<section>
  <div class="contacts">
    <div class="row">
      <div class="title"><span><?php the_field('contacts_title', pll_current_language('slug'));?></span></div>
      <div class="contacts-address clearfix">
        <div class="bases column large-6 medium-12">
          <div class="bases-title"><span><?php _e('bases','lionline');?></span></div>
          <div class="bases-items clearfix">
            <?php if( have_rows('bases', pll_current_language('slug')) ):?>
              <?php $id=0; ?>
              <?php while ( have_rows('bases', pll_current_language('slug')) ) : ?>
                <?php the_row(); ?>
                <?php if( get_sub_field('show_home', pll_current_language('slug')) ):?>
                  <div class="bases-item column large-6 medium-6">
                    <div class="bases-item__contact">
                      <div class="bases-item__city">
                       <i class="fas fa-map-marker-alt"></i>
                       <a href="#map-canvas" class="marker-link_base" data-markerid="<?php echo $id ;?>"><?php the_sub_field('city', pll_current_language('slug'));?></a>
                     </div>
                     <div class="bases-item__address">
                       <p><?php _e('contacts adress','lionline');?></p>
                       <a class="marker-link_base" href="#map-canvas" data-markerid="<?php echo $id ;?>"><?php the_sub_field('address', pll_current_language('slug'));?></a>
                     </div>
                   </div>
                   <div class="bases-item__work-time">
                    <div class="work-time">
                      <p><?php _e('contacts work-time','lionline');?></p>
                      <?php
                      $field=get_sub_field('work_time');
                      $lines = explode("\n", $field);
                      if ( !empty($lines) ) {
                        echo '<ul>';
                        foreach ( $lines as $line ) {
                         echo '<li>'. trim( $line ) .'</li>';
                       }
                       echo '</ul>';
                     }
                     ?>
                   </div>
                   <div class="bases-item__phone">
                    <p>Тел:</p><a href="tel:<?php the_sub_field('bases_phone', pll_current_language('slug'));?>"><?php the_sub_field('bases_phone', pll_current_language('slug'));?></a>
                  </div>
                </div>
                <div class="bases-item__btn">
                  <a href="<?php the_sub_field('link', pll_current_language('slug'));?>"><?php the_sub_field('link_text', pll_current_language('slug'));?>
                  <i class="fa fa-chevron-right" aria-hidden="true"></i>
                  <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </a>
              </div>
            </div>
            <?php $id++;?>
          <?php endif; ?>
        <?php  endwhile; ?>
      <?php endif; ?>
    </div>
    <div class="all-bases"><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i><?php _e('all bases','lionline');?></a></div>
  </div>

  <div class="partners column large-6 medium-12">
    <div class="partners-title"><span><?php _e('partners','lionline');?></span></div>
    <div class="partners-items">
      <?php if( have_rows('partners_base', pll_current_language('slug')) ):?>
        <?php $id=0; ?>
        <?php while ( have_rows('partners_base', pll_current_language('slug')) ) : ?>
          <?php the_row(); ?>
          <?php if( get_sub_field('show_home', pll_current_language('slug')) ):?>
            <div class="partners-item large-6 medium-4">
              <div class="partners-item__name"><a class="marker-link_partner" href="#map-canvas" data-markerid="<?php echo $id ;?>"><?php the_sub_field('partners_name', pll_current_language('slug'));?></a></div>
              <div class="partners-item__address">
                <p><?php _e('contacts adress','lionline');?></p><a class="marker-link_partner" href="#map-canvas" data-markerid="<?php echo $id ;?>"><?php the_sub_field('address', pll_current_language('slug'));?></a>
              </div>
              <div class="partners-item__phone">
                <p>Тел:</p><a href="tel:<?php the_sub_field('bases_phone', pll_current_language('slug'));?>"><?php the_sub_field('bases_phone', pll_current_language('slug'));?></a>
              </div>
              <div class="bases-item__btn">
                <a href="<?php the_sub_field('link', pll_current_language('slug'));?>"><?php the_sub_field('link_text', pll_current_language('slug'));?>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
              </a>
            </div>
          </div>
          <?php $id++;?>
        <?php endif; ?>
      <?php  endwhile; ?>
    <?php endif; ?>
  </div>
  <div class="all-bases"><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i><?php _e('all partners','lionline');?></a></div>
</div>
</div>
</div>

<div class="map">
  <div id="markers"></div>
  <div id="map-canvas">
    <script>
      function initialize() {
        var markers = new Array();
        var markers_partner = new Array();
        var mapOptions = {
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: new google.maps.LatLng(50.450657,  30.522930),
        };
        var locations = [
        <?php if( have_rows('bases', pll_current_language('slug')) ):?>
          <?php $id=0; ?>
          <?php while ( have_rows('bases', pll_current_language('slug')) ) : ?>
            <?php the_row(); ?>
            <?php if( get_sub_field('show_home', pll_current_language('slug')) ):?>
              <?php
              $items=get_sub_field('coordinates', pll_current_language('slug'));
              ?>
              [new google.maps.LatLng(<?php echo $items['lat'] ?>, <?php echo $items['lng'] ?>),'<div class="info_block"><div class="logo"><img src="<?php echo get_template_directory_uri();?>/dist/images/logo.png"></div><span><?php the_sub_field('city', pll_current_language('slug'));?></span><p><?php echo str_ireplace(array("\r","\n",'\r','\n'),'',get_sub_field('address'));?></p><a href="tel:<?php the_sub_field('bases_phone', pll_current_language('slug'));?>"><?php the_sub_field('bases_phone', pll_current_language('slug'));?></a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],
              <?php $id++;?>
            <?php endif; ?>
          <?php  endwhile; ?>
        <?php endif; ?>
        ];

        var locations_partner = [
        <?php if( have_rows('partners_base', pll_current_language('slug')) ):?>
          <?php $id=0; ?>
          <?php while ( have_rows('partners_base', pll_current_language('slug')) ) : ?>
            <?php the_row(); ?>
            <?php if( get_sub_field('show_home', pll_current_language('slug')) ):?>
              <?php
              $items=get_sub_field('coordinates_part', pll_current_language('slug'));
              ?>
              [new google.maps.LatLng(<?php echo $items['lat'] ?>, <?php echo $items['lng'] ?>),'<div class="info_block"><span><?php the_sub_field('partners_name', pll_current_language('slug'));?></span><p><?php echo str_ireplace(array("\r","\n",'\r','\n'),'',get_sub_field('address'));?></p><a href="tel:<?php the_sub_field('bases_phone', pll_current_language('slug'));?>"><?php the_sub_field('bases_phone', pll_current_language('slug'));?></a></div>','<?php echo get_template_directory_uri();?>/dist/images/map_marker.svg'],
              <?php $id++;?>
            <?php endif; ?>
          <?php  endwhile; ?>
        <?php endif; ?>


        ];

        console.log(locations_partner);
        console.log(locations);

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var infowindow = new google.maps.InfoWindow();
        for (var i = 0; i < locations_partner.length; i++) {

          // Append a link to the markers DIV for each marker
          // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

          var marker = new google.maps.Marker({
            position: locations_partner[i][0],
            map: map,
            icon:locations_partner[i][2],
            title: locations_partner[i][1],
          });

          // Register a click event listener on the marker to display the corresponding infowindow content
          google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
              infowindow.setContent(locations_partner[i][1]);
              infowindow.open(map, marker);
            };

          })(marker, i));

          // Add marker to markers array
          markers_partner.push(marker);
        }

        for (var i = 0; i < locations.length; i++) {

          // Append a link to the markers DIV for each marker
          // $('#markers').append('<a class="marker-link" data-markerid="' + i + '" href="#">' + locations[i][1] + '</a> ');

          var marker = new google.maps.Marker({
            position: locations[i][0],
            map: map,
            icon:locations[i][2],
            title: locations[i][1],
          });

          // Register a click event listener on the marker to display the corresponding infowindow content
          google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {
              infowindow.setContent(locations[i][1]);
              infowindow.open(map, marker);
            };

          })(marker, i));

          // Add marker to markers array
          markers.push(marker);
        }

        // Trigger a click event on each marker when the corresponding marker link is clicked
        jQuery('.marker-link_base').on('click', function (e) {
          e.preventDefault();

          var top = jQuery("#map-canvas").offset().top-50;
          jQuery('body,html').animate({scrollTop: top}, 1000);

          google.maps.event.trigger(markers[jQuery(this).data('markerid')], 'click');
        });
        jQuery('.marker-link_partner').on('click', function (e) {
          e.preventDefault();
          var top = jQuery("#map-canvas").offset().top-50;
          jQuery('body,html').animate({scrollTop: top}, 1000);
          google.maps.event.trigger(markers_partner[jQuery(this).data('markerid')], 'click');
        });
      }
    </script>

    <script  defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApj8qSzyO4DqXeQtFQPKI4mi6p-MBwlpo&amp;callback=initialize"></script>
  </div>
</div>
</div>
</section>
