<section>
	<div class="slider-partners">
		<!-- <div class="row"> -->
			<div class="title">
				<span><?php the_field('partners_title',pll_current_language('slug'));?></span>
			</div>
			<div class="partners__slider">

				<?php if( have_rows('partners',pll_current_language('slug')) ):?>
					<?php while ( have_rows('partners',pll_current_language('slug')) ) : ?>
						<?php the_row(); ?>
						<img src="<?php the_sub_field('partners_logo',pll_current_language('slug'));?>" alt="">
					<?php  endwhile; ?>
				<?php endif; ?>

			</div>
		<!-- </div> -->
	</div>
</section>