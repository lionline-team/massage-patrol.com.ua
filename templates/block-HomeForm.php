<section>
	<div class="form-wrap">
		<div class="row">
			<div class="title"><span><?php _e('home form title','lionline');?></span>
				<div class="sub-title"><span><?php _e('home form sub-title','lionline');?></span></div>
			</div>
			<div class="form large-10 large-offset-1">
				<form class='homecontact'>
					<div class="form__inputs column large-7 medium-6">
						<p><?php _e('form name','lionline');?></p>
						<input class= "contactname" type="text" name="name" placeholder="<?php _e('form name','lionline');?>">
						<p><?php _e('form email or phone','lionline');?></p>
						<input class="contactemail" type="text" name="email" placeholder="<?php _e('form email or phone','lionline');?>" required>
						<p><?php _e('form Message','lionline');?></p>
						<textarea class="contacttext" name="masseg" placeholder="<?php _e('form Message','lionline');?>"></textarea>
					</div>
					<div class="form__foto column large-5 medium-6 hide-for-small-only"><img src="<?php echo get_template_directory_uri();?>/dist/images/slideritem2.jpg" alt=""></div>
					<div class="form__btn column">
						<button class="btn button success" type="submit"><?php _e('form button','lionline');?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script>
jQuery( document ).ready(function() {

    if ( typeof contact_form_home_hook_defined === 'undefined') { // No dublicate hooks, when use two forms on one page

   

      jQuery(document).on('submit','form.homecontact',function(e){
        var name = jQuery(this).find('input.contactname').val();
        var email = jQuery(this).find('input.contactemail').val();
        var text = jQuery(this).find('textarea.contacttext').val();
        var to = 'admin';

        // ЗАПУСКАЙ АНІМАЦІЮ ТУТ

        // animateButton(jQuery(this).find('button'));

        jQuery(this).find('button').addClass('animate');
        jQuery(".homecontact").trigger("reset");
        
        jQuery.ajax({
          url: ajaxurl,
          data: {
            'action':'sendmsg',
            'name':name,
            'email':email,
            'text':text,
            'to':to,
            'subscribe':'',
          },
          success:function(data) {
            console.log('sended!');

            console.log(data);
            setTimeout(function(){
              jQuery('button').removeClass('animate');
            },3000);
            // ЗУПИНЯЙ АНІМАЦІЮ ТУТ
          },
          error: function(errorThrown){
            console.log(errorThrown);
          }
        }); 
        e.preventDefault(e);
      });
      contact_form_home_hook_defined=true;

    }
  });

</script>